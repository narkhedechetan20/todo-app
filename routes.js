// contains all connections and routes
import ProductRouter from './Product/product.routes.js'

export const routes = [{
    path: '/product',
    handler: ProductRouter
}]
import ProductRepo from './product.repo.js';

const getAllProducts = () => ProductRepo.getAllProducts();

const getProductById = (id) =>  ProductRepo.getProductById(id);

const createProduct = (Product) => ProductRepo.createProduct(Product);

const updateProduct = (id) => ProductRepo.updateProduct(id);

const deleteproduct = (id) => ProductRepo.deleteproduct(id);

export default {
    getAllProducts,
    getProductById,
    createProduct,
    updateProduct,
    deleteproduct
}
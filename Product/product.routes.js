import { query, Router } from 'express';
import ProductService from './product.service.js';

const router = Router();

// read --> GET
//GET ALL Produtcs
//route == http://localhost:5000/product/
router.get("/",(req,res,next)=> {
    const result = ProductService.getAllProducts();
    res.send(result)
});

//Read ---> GET
//flow Error.
//Static path should be declared before dynamic paths
//query parameters are going to be passed to this api
//route == http://localhost:5000/product/getById?prodcutId=101
router.get("/getById",(req,res,next)=> {
    console.log(req.query)
    const { productId } = req.query;
    const result = ProductService.getProductById(productId);
    res.send(result)
});

//GET one Product(using ID/Name)
//route == http://localhost:5000/product/101 ==> (productId = 101)
{
    productId: 100
}
router.get("/:productId",(req,res,next)=> { 
    console.log(req.params)
    const { productId } = req.params;
    const result = ProductService.getProductById(productId);
    res.send(result)
});


//Create Part --> POST method
router.post("/",(req,res,next)  => {
    console.log(req.body)
    const product = req.body;
    const result = ProductService.createProduct(product)
    res.send(result)
});


//Update Part --> PUT/PATCH method
router.put("/",(req, res, next) => {
    console.log(req.body)
    const productToUpdate = req.body;
    const result = ProductService.updateProduct(productToUpdate)
    res.send(result)
});


//Delete Part --> DELETE method
router.delete("/:productId", (req, res, next) => {
    console.log(req.params)
    const { productId } = req.params;
    const result = ProductService.deleteproduct(productId);
    res.send(result);
});

export default router;
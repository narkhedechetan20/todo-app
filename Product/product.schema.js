import { v4 as uuidv4 } from 'uuid';

class ProductSchema {

    constructor(products) {
        this.products = products;
    }

    getAll() {
        return this.products;
    }

    getByID(id) {
        return this.products.find(product => product.is === id);
    }

    create(product) {
        //product.id = this.products.length;
        //Better way to create new id 
        product.id = uuidv4();
        this.products.push(product);
        return product
    }

    update(productToUpdate) {
        const productIndex = this.products.findIndex(product => product.id === productToUpdate.id)
        if (productIndex !== -1){
            this.products[productIndex] = productToUpdate
        } else {
            throw {message: "Product Not Found"}
        }
    }

    delete(id) {
        const productIndex = this.products
            .findIndex(product => product.id === id)
            if (productIndex !== -1){
                this.products.splice(productIndex, 1)
            } else {
                throw {message: "Product Not Found"}
            }
    }
}

const productModel = new ProductSchema([]);

export default productModel;
import productModel from './product.schema.js';

const getAllProducts = () => productModel.getAll();

const getProductById = (id) =>  productModel.getByID(id);

const createProduct = (Product) => productModel.create(Product);

const updateProduct = (product) => productModel.update(product);

const deleteproduct = (id) => productModel.delete(id);

export default {
    getAllProducts,
    getProductById,
    createProduct,
    updateProduct,
    deleteproduct
}
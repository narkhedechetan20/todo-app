import helmet from 'helmet';
import { json } from "express";
import { routes } from './routes.js'

export const registerMiddlewares = (app) => {
    //registering all 'Universal' middlewares
    app.use(json()); // Parses incoming body which is of type JSON
    app.use(helmet())

    app.use((req, res, next) => {
        console.log("Server was Accessed")
        next();
    });

    // registering routes in application 
    for(let route of routes) {
        app.use(route.path, route.handler);
    }
    
    // error handling middleware
    app.use((err, req, res, next) => {

    });
}
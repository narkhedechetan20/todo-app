import express from 'express';
import { registerMiddlewares } from './registerMiddleware.js';

export const startServer = () => {
    const app = express()
    registerMiddlewares(app)

    app.listen(
        process.env.PORT, 
        () => console.log(`Server Started on ${process.env.PORT}`)
    );
}